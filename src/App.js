import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import rootReducer from './reducer';
import Root from './components/Root';
import MainScreen from './main';
import LearnScreen from './learn';
import ManageScreen from './manage';
import ProfileScreen from './profile';
import NotesScreen from './notes';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
    rootReducer,
    composeEnhancers(
      applyMiddleware(thunk)
    )
);

function App() {
    return (
        <Provider store={store}>
            <Root>
                <Router>
                    <Route path="/" exact component={MainScreen} />
                    <Route path="/learn" component={LearnScreen} />
                    <Route path="/manage" component={ManageScreen} />
                    <Route path="/profile" component={ProfileScreen} />
                    <Route path="/notes" component={NotesScreen} />
                </Router>
            </Root>
        </Provider>
    );
}

export default App;
