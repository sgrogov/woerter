export default {
    maskulinum: [
        {
            categories: [
                ['male', 'animated'],
            ],
        }, {
            categories: [['male', 'profession']]
        }, {
            categories: ['fish'],
            exceptions: [
                {
                    value: 'forelle',
                    gender: 'femininum',
                }, {
                    value: 'sardine',
                    gender: 'femininum',
                }, {
                    value: 'plötze',
                    gender: 'femininum',
                }, {
                    value: 'karausche',
                    gender: 'femininum',
                },
            ],
        }, {
            categories: ['birds'],
            exceptions: [
                {
                    value: 'gans',
                    gender: 'femininum',
                }, {
                    value: 'nachtigall',
                    gender: 'femininum',
                }, {
                    value: 'drossel',
                    gender: 'femininum',
                }, {
                    value: 'schwalbe',
                    gender: 'femininum',
                }, {
                    value: 'elster',
                    gender: 'femininum',
                }, {
                    value: 'möwe',
                    gender: 'femininum',
                },
            ],
        }, {
            categories: ['seasons', 'weekdays', 'months', 'daytime'],
            exceptions: [
                {
                    value: 'nacht',
                    gender: 'femininum',
                },
            ],
        }, {
            categories: 'cardinal directions',
        }, {
            categories: ['precipitation', 'winds'], // осадки
            exceptions: [
                {
                    value: 'brise',
                    gender: 'femininum',
                }, {
                    value: 'bora',
                    gender: 'femininum',
                },
            ],
        }, {
            categories: ['jewels', 'rock'],
            exceptions: [
                {
                    value: 'kreide',
                    gender: 'femininum',
                }, {
                    value: 'perle',
                    gender: 'femininum',
                },
            ],
        }, {
            categories: ['mountain', 'sierra'],
        }, {
            categories: ['auto'],
        }, {
            categories: ['currency units', 'coins'],
            exceptions: [
                {
                    value: 'kopeke',
                    gender: 'femininum',
                }, {
                    value: 'krone',
                    gender: 'femininum',
                }, {
                    value: 'lira',
                    gender: 'femininum',
                }, {
                    value: 'pfund',
                    gender: 'femininum',
                },
            ],
        }, {
            categories: 'drinks',
            exceptions: [
                {
                    value: 'bier',
                    gender: 'neutrum',
                }, {
                    value: 'wasser',
                    gender: 'neutrum',
                }, {
                    value: 'limonade',
                    gender: 'femininum',
                }, {
                    value: 'vodka',
                    gender: 'femininum',
                },
            ],
        }, {
            categories: ['animated'],
            suffix: 'er',
        }, {
            categories: ['animated'],
            suffix: 'ler',
        }, {
            categories: ['animated'],
            suffix: 'ner',
        }, {
            suffix: 'ling',
        }, {
            suffix: 'ig',
            exceptions: [
                {
                    value: 'reisig',
                    gender: 'neutrum',
                }
            ],
        }, {
            categories: ['foreign'],
            suffix: [
                'ismus',
                'us',
                'ant',
                'and',
                'ent',
                'at',
                'it',
                'ot',
                'et',
                'ist',
                'log',
                'loge',
                'graph',
                'ier',
                'eur',
                'ar',
                'är',
                'al',
                'or',
                'ast',
            ],
        }
    ],
    femininum: [
        {
            categories: [
                ['female', 'animated'],
            ],
            exceptions: [
                {
                    value: 'weib',
                    gender: 'neutrum',
                }, {
                    value: 'fräulein',
                    gender: 'neutrum',
                }, {
                    value: 'mädchen',
                    gender: 'neutrum',
                }, {
                    value: 'schaf',
                    gender: 'neutrum',
                }, {
                    value: 'huhn',
                    gender: 'neutrum',
                }, {
                    value: 'reh',
                    gender: 'neutrum',
                }, {
                    value: 'frosch',
                    gender: 'maskulinum',
                }, {
                    value: 'panter',
                    gender: 'maskulinum',
                },
            ],
        }, {
            categories: 'insects',
            exceptions: [
                {
                    value: 'floh',
                    gender: 'maskulinum',
                }, {
                    value: 'käfer',
                    gender: 'maskulinum',
                }, {
                    value: 'skorpion',
                    gender: 'maskulinum',
                },
            ],
        }, {
            categories: [
                ['female', 'profession'],
            ],
        }, {
            categories: 'tree',
            exceptions: [
                {
                    value: 'ahorn',
                    gender: 'maskulinum',
                }, {
                    value: 'lorbeer',
                    gender: 'maskulinum',
                }, {
                    value: 'baobab',
                    gender: 'maskulinum',
                },
            ],
        }, {
            categories: 'flowers',
            exceptions: [
                {
                    value: 'mohn',
                    gender: 'maskulinum',
                }, {
                    value: 'jasmin',
                    gender: 'maskulinum',
                }, {
                    value: 'flieder',
                    gender: 'maskulinum',
                }, {
                    value: 'phlox',
                    gender: 'maskulinum',
                }, {
                    value: 'kaktus',
                    gender: 'maskulinum',
                }, {
                    value: 'vergissmeinnich',
                    gender: 'neutrum',
                }, {
                    value: 'veilchen',
                    gender: 'neutrum',
                }, {
                    value: 'maiglöckchen',
                    gender: 'neutrum',
                },
            ],
        }, {
            categories: ['fruits', 'vegetables'],
            exceptions: [
                {
                    value: 'apfel',
                    gender: 'maskulinum',
                }, {
                    value: 'pfirsich',
                    gender: 'maskulinum',
                }, {
                    value: 'kurbis',
                    gender: 'maskulinum',
                }, {
                    value: 'kohl',
                    gender: 'maskulinum',
                }, {
                    value: 'knoblauch',
                    gender: 'maskulinum',
                }, {
                    value: 'rettich',
                    gender: 'maskulinum',
                }, {
                    value: 'spargel',
                    gender: 'maskulinum',
                },
            ],
        }, {
            categories: 'numerals',
        }, {
            categories: 'german rivers',
            exceptions: [
                {
                    value: 'rhein',
                    gender: 'maskulinum',
                }, {
                    value: 'main',
                    gender: 'maskulinum',
                }, {
                    value: 'nechar',
                    gender: 'maskulinum',
                },
            ],
        }, {
            categories: ['aircrafts', 'ships', 'motorcycles'],
        }, {
            categories: 'animated',
            suffix: ['in', 'ung', 'heit', 'keit', 'schaft', 'ei'],
        }, {
            categories: 'foreign',
            suffix: ['ie', 'tät', 'ität', 'ion', 'tion', 'ik', 'ur', 'enz', 'age', 'ade', 'anz', 'ive', 'thek', 'üre', 'ine', 'itis'],
        },
    ],
    neutrum: [
        {
            categories: ['children and cubs'],
            exceptions: [
                {
                    value: 'welpe',
                    gender: 'maskulinum',
                },
            ],
        }, {
            categories: ['continents', 'countries', 'cities', 'islands'],
            exceptions: [
                {
                    value: 'arktis',
                    gender: 'femininum',
                }, {
                    value: 'antarktis',
                    gender: 'femininum',
                }, {
                    value: 'krim',
                    gender: 'femininum',
                }, {
                    value: 'normandie',
                    gender: 'femininum',
                }, {
                    value: 'schweiz',
                    gender: 'femininum',
                }, {
                    value: 'ukraine',
                    gender: 'femininum',
                }, {
                    value: 'irak',
                    gender: 'maskulinum',
                }, {
                    value: 'iran',
                    gender: 'maskulinum',
                }, {
                    value: 'haag',
                    gender: 'maskulinum',
                }, {
                    value: 'vatikan',
                    gender: 'maskulinum',
                },
            ],
        }, {
            categories: 'metal',
            exceptions: [
                {
                    value: 'bronze',
                    gender: 'femininum',
                }, {
                    value: 'stahl',
                    gender: 'maskulinum',
                }, {
                    value: 'phosphor',
                    gender: 'maskulinum',
                }, {
                    value: 'schwefel',
                    gender: 'maskulinum',
                },
            ],
        }, {
            categories: ['hotels', 'cinemas', 'cafe'],
        }, {
            categories: ['letters', 'music note', 'colors', 'languages'],
        }, {
            categories: 'detergent',
        }, {
            // субстантивированные инфинитивы
        }, {
            // субстантивированные прилагательные и причастия
        }, {
            // другие субстантивированные части речи
        }, {
            suffix: [
                'chen',
                'lein',
                'sal',
                'sel',
                'nis',
                'tum',
                'tel',
                'stel',
            ],
        }, {
            categories: 'foreign',
            suffix: [
                'um',
                'ium',
                'ment',
                'ett',
                'il',
                'in',
                'ma',
                'o',
            ],
        }, {
            categories: 'foreign',
            suffix: [
                'ment',
            ],
            exceptions: [
                {
                    value: 'zement',
                    gender: 'maskulinum',
                },
            ],
        }, {
            prefix: 'ge',
            exceptions: [
                {
                    value: 'gebrauch',
                    gender: 'maskulinum',
                }, {
                    value: 'geschmack',
                    gender: 'maskulinum',
                }, {
                    value: 'geruch',
                    gender: 'maskulinum',
                }, {
                    value: 'gesang',
                    gender: 'maskulinum',
                },
            ],
        }
    ],
};
