export function findMatchedRules(word, genderRules) {
    const result = {};
    for (const [gender, rules] of Object.entries(genderRules)) {
        for (const rule of rules) {
            const matches = matchesToRule(word, rule);
            if (matches) {
                if (result[gender] === undefined) {
                    result[gender] = [];
                }
                result[gender].push(rule);
            }
        }
    }
    return result;
}

function matchesToRule(word, rule) {
    return !isRuleEmpty(rule)
        // && checkCategories(word, rule)
        && (
            checkSuffixes(word, rule) || checkPrefixes(word, rule)
        )
        // && checkExceptions(word, rule);
}

function isRuleEmpty(rule) {
    return Object.keys(rule).length === 0;
}

function getWordCategories(word) {
    return new Set();
}

function checkCategories(word, rule) {
    if (!Object.hasOwnProperty.call(rule, 'categories')) return true;

    const existingCategories = getWordCategories(word);
    const categories = Array.isArray(rule.categories)
        ? rule.categories
        : [rule.categories];

    return categories.some((_subcategories) => {
        const subcategories = Array.isArray(_subcategories)
            ? _subcategories
            : [_subcategories];

        return subcategories.every(category => existingCategories.has(category));
    });
}

function checkSuffixes(word, rule) {
    if (!Object.hasOwnProperty.call(rule, 'suffix')) return false;

    const suffixes = Array.isArray(rule.suffix)
        ? rule.suffix
        : [rule.suffix];

    return suffixes.some(suffix => checkSuffix(word, suffix));
}
function checkSuffix(word, suffix) {
    return word.substr(-suffix.length) === suffix;
}

function checkPrefixes(word, rule) {
    if (!Object.hasOwnProperty.call(rule, 'prefix')) return false;

    const prefixes = Array.isArray(rule.prefix)
        ? rule.prefix
        : [rule.prefix];

    return prefixes.some(prefix => checkPrefix(word, prefix));
}
function checkPrefix(word, prefix) {
    return word.substr(prefix.length) === prefix;
}

function checkExceptions(word, rule) {
    if (!Object.hasOwnProperty.call(rule, 'exceptions')) return true;

    return rule.exceptions.some(exception => exception.value === word);
}
