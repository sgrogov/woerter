import React from 'react';
import get from 'lodash/get';
import has from 'lodash/has';
import Screen from '../components/Screen';
import rules from './rules';
import {findMatchedRules} from './utils';

const categories = {
    male: {
        emoji: '♂️',
    },
    female: {
        emoji: '♀️',
    },
    animated: {
        emoji: '',
    },
    profession: {
        emoji: '👨‍🍳',
    },
    fish: {
        emoji: '🐟',
    },
    birds: {
        emoji: '🦜',
    },
    seasons: {
        emoji: '(☀️🍂❄️🌱)',
    },
    weekdays: {
        emoji: '📅',
    },
    months: {
        emoji: '📅',
    },
    daytime: {
        emoji: '🌅',
    },
    'cardinal directions': {
        emoji: '🧭',
    },
    precipitation: {
        emoji: '⛈️',
    },
    winds: {
        emoji: '🌬️',
    },
    jewels: {
        emoji: '💎',
    },
    rock: {
        emoji: '⛏️',
    },
    mountain: {
        emoji: '⛰️',
    },
    sierra: {
        emoji: '🏔️',
    },
    auto: {
        emoji: '🚗',
    },
    'currency units': {
        emoji: '💲',
    },
    coins: {
        emoji: '💰',
    },
    drinks: {
        emoji: '🥤',
    },
    foreign: {
        emoji: '🏳️',
    },
    insects: {
        emoji: '🦟',
    },
    tree: {
        emoji: '🌳',
    },
    flowers: {
        emoji: '🌷',
    },
    fruits: {
        emoji: '🍏',
    },
    vegetables: {
        emoji: '🥕',
    },
    numerals: {
        emoji: '1️⃣',
    },
    'german rivers': {
        emoji: '🇩🇪🌊',
    },
    aircrafts: {
        emoji: '✈️',
    },
    ships: {
        emoji: '🚢',
    },
    motorcycles: {
        emoji: '🏍️',
    },
    'children and cubs': {
        emoji: '👶',
    },
    continents: {
        emoji: '🗺️',
    },
    countries: {
        emoji: '🗺️',
    },
    cities: {
        emoji: '🗺️',
    },
    islands: {
        emoji: '🏝️',
    },
    metal: {
        emoji: '⛏️',
    },
    hotels: {
        emoji: '🏨',
    },
    cinemas: {
        emoji: '🎦',
    },
    cafe: {
        emoji: '☕',
    },
    letters: {
        emoji: '🔠',
    },
    'music note': {
        emoji: '🎶',
    },
    colors: {
        emoji: '🎨',
    },
    languages: {
        emoji: '🗣️',
    },
    detergent: {
        emoji: '🧼',
    },
};

const categoryToEmoji = category => get(categories, `${category}.emoji`, '?');
const wrapToArray = value => Array.isArray(value) ? value : [value];

export default class NotesScreen extends React.Component {
    state = {
        word: '',
    };

    onChange = (e) => {
        this.setState({
            word: e.target.value,
        });
    };

    renderCategories(rule) {
        return wrapToArray(rule.categories).map(subcategories => {
            return wrapToArray(subcategories).map(categoryToEmoji).join('');
        }).join(', ');
    }
    renderPrefixes(rule) {
        return wrapToArray(rule.prefix)
            .map(prefix => `${prefix}-`)
            .join(', ');
    }
    renderSuffixes(rule) {
        return wrapToArray(rule.suffix)
            .map(suffix => `-${suffix}`)
            .join(', ');
    }

    renderRules(rules) {
        return (
            <ul>
                {rules.map((rule, i) => this.renderRule(rule, i))}
            </ul>
        );
    }

    renderRule(rule, key) {
        return (
            <li key={key}>
                {has(rule, 'categories') && (
                    <p>{this.renderCategories(rule)}</p>
                )}
                {has(rule, 'prefix') && (
                    <p>{this.renderPrefixes(rule)}</p>
                )}
                {has(rule, 'suffix') && (
                    <p>{this.renderSuffixes(rule)}</p>
                )}
            </li>
        );
    }

    render() {
        const matchedRules = findMatchedRules(this.state.word, rules);

        return (
            <Screen
                name="notes"
                heading="Notes"
                backTo="/"
            >
                <input type="text" onChange={this.onChange} />
                {Object.entries(matchedRules).map(([gender, rules]) => (
                    <React.Fragment key={gender}>
                        <p style={{textTransform: 'capitalize'}}>{gender}</p>

                        {this.renderRules(rules)}
                    </React.Fragment>
                ))}
                {Object.entries(rules).map(([gender, rules]) => (
                    <React.Fragment key={gender}>
                        <p style={{textTransform: 'capitalize'}}>{gender}</p>

                        {this.renderRules(rules)}
                    </React.Fragment>
                ))}
            </Screen>
        );
    }
}
