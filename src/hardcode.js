export const languages = [
    'de',
    'en',
    'ru',
];

export const genders = {
    de: ['m', 'f', 'n'],
    en: [],
    ru: ['m', 'f', 'n'],
};

export const pronounTypes = [
    'personal',
    'possessive',
    'reflexive',
];

export const partsOfSpeech = [
    {
        name: 'noun',
        fields: [
            'gender',
        ],
    }, {
        name: 'verb',
    }, {
        name: 'adjective',
    }, {
        name: 'pronoun',
        fields: [
            'pronounType'
        ],
    }
];
