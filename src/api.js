import axios from 'axios';

const matches = /^([a-z.]+)|(\d+\.\d+\.\d+\.\d+).*$/.exec(window.location.host);
const host = matches[1] || matches[2];


let a;
const createInstance = (token) => {
    const headers = {};
    if (typeof token === 'string') {
        headers.authorization = token;
    }
    a = axios.create({
        baseURL: `http://${host}:3000/`,
        headers,
    });
};
createInstance(localStorage.getItem('authorization'));

export default {
    profile: {
        status: async () => (await a.get('/users/status')).data,
        login: async (email) => {
            const response = await a.post('/users/login', {
                email: email,
            });
            createInstance(response.headers.authorization);
            localStorage.setItem('authorization', response.headers.authorization);
            return response.data;
        },
    },
    learn: {
        exercise: async () => {
            const response = await a.get('/exercises');
            return response.data;
        },
    },
    words: {
        add: async (word) => (await a.post('/words/add', word)).data,
        list: async () => (await a.get('/words/list')).data,
    },
};
