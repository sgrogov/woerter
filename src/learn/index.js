import React from 'react';
import { connect } from 'react-redux';
import Screen from '../components/Screen';
import { exercise } from './reducer';

class LearnScreen extends React.Component {
    componentDidMount() {
        this.props.fetchExercise();
    }

    render() {
        return (
            <Screen
                name="learn"
                heading="Learn"
                backTo="/"
            >
                Learn
            </Screen>
        );
    }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({
    fetchExercise: () => exercise(dispatch),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(LearnScreen);
