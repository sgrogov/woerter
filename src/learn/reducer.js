import Api from '../api';

const initialState = {
    exercise: null,
};

const EXERCISE_REQUEST = 'LEARN/EXERCISE_REQUEST';
const EXERCISE_SUCCESS = 'LEARN/EXERCISE_SUCCESS';
const EXERCISE_ERROR = 'LEARN/EXERCISE_ERROR';

const exerciseRequest = () => ({ type: EXERCISE_REQUEST });
const exerciseSuccess = (exercise) => ({
    type: EXERCISE_SUCCESS,
    payload: { exercise },
});
const exerciseError = () => ({ type: EXERCISE_ERROR });

export const exercise = async (dispatch) => {
    dispatch(exerciseRequest());
    try {
        const exercise = await Api.learn.exercise();
        dispatch(exerciseSuccess(exercise));
    } catch (e) {
        console.error(e);
        dispatch(exerciseError());
    }
};

export default function learnReducer(state = initialState, action) {
    switch (action.type) {
        case EXERCISE_SUCCESS:
            return {
                ...state,
                exercise: action.payload.exercise,
            };
        case EXERCISE_ERROR:
            return {
                ...state,
                exercise: null,
            };
        default:
            return state;
    }
}
