import React from 'react';
import styles from './settings.module.css';

const Label = ({ id, text }) => (
    <label
        htmlFor={id}
        className={styles.label}
    >{text}</label>
);

const Field = (props) => (
    <div className={styles.field}>
        <props.component
            id={props.id}
            value={props.value}
            options={props.options}
            onChange={props.onChange}
        />
    </div>
);

const SelectInput = ({ id, options, value, onChange }) => (
    <select
        className={styles.select}
        id={id}
        value={value}
        onChange={e => onChange(e.target.value)}
    >
        {options.map(option => (
            <option
                key={option.value}
                value={option.value}
            >{option.text}</option>
        ))}
    </select>
);

const TextInput = ({ id, value, onChange }) => (
    <input
        className={styles.text}
        type="text"
        id={id}
        value={value}
        onChange={onChange}
    />
);

export default class Settings extends React.Component {
    state = {
        value: {},
    };

    handlers = new Map();

    getComponent(type = 'text') {
        switch (type) {
            case 'select':
                return SelectInput;
            default:
                return TextInput;
        }
    }

    getFieldChangeHandler(id) {
        let handler = this.handlers.get(id);
        if (handler === undefined) {
            handler = value => this.setState(prevState => ({
                value: {
                    ...prevState.value,
                    [id]: value,
                },
            }));
        }
        return handler;
    }

    render() {
        return (
            <div className={styles.container}>
                {this.props.items.map(item => ([
                    <Label
                        key={`label-${item.id}`}
                        id={item.id}
                        text={item.text}
                    />,
                    <Field
                        key={`input-${item.id}`}
                        id={item.id}
                        value={Object.hasOwnProperty.call(this.state.value, item.id)
                            ? this.state.value[item.id]
                            : this.props.value[item.id]}
                        options={item.options}
                        component={this.getComponent(item.type)}
                        onChange={this.getFieldChangeHandler(item.id)}
                    />
                ]))}
            </div>
        )
    }
}
