import React from 'react';
import cn from 'classnames';
import styles from './button.module.css';

export default class Button extends React.Component {
    render() {
        return (
            <button
                {...this.props}
                className={cn(styles.container, this.props.className)}
            />
        );
    }
}
