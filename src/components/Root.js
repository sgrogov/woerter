import React from 'react';
import { connect } from 'react-redux';
import { status } from '../profile/reducer';

const mapDispatchToProps = dispatch => ({
    onMount() {
        status(dispatch);
    },
});

class Root extends React.Component {
    componentDidMount() {
        this.props.onMount();
    }

    render() {
        return this.props.children;
    }
}

export default connect(null, mapDispatchToProps)(Root);
