import React from 'react';
import { Link } from 'react-router-dom';
import styles from './screen.module.css';

export default class Screen extends React.Component {
    componentDidMount() {
        document.querySelector('#root').scrollTop = 0;
    }

    render() {
        return (
            <div className={styles.container}>
                <div className={styles.head}>
                    {typeof this.props.backTo === 'string' && (
                        <Link
                            className={styles.back}
                            to={this.props.backTo}
                        >{'<'}</Link>
                    )}
                    {typeof this.props.heading === 'string' && (
                        <h1 className={styles.heading}>{this.props.heading}</h1>
                    )}
                </div>
                {this.props.children}
            </div>
        );
    }
}
