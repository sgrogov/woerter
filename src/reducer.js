import { combineReducers } from 'redux';

import profile from './profile/reducer';
import learn from './learn/reducer';

export default combineReducers({
    profile,
    learn,
});
