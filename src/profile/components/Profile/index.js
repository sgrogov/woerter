import React from 'react';
import { connect } from 'react-redux';
import styles from './profile.module.css';
import Settings from '../../../components/Settings';
import { selectProfile } from '../../reducer';

class Profile extends React.Component {
    settings = [
        {
            id: 'target-language',
            type: 'select',
            text: 'Target language',
            options: [
                { value: 'en', text: 'English' },
                { value: 'de', text: 'Deutsch' },
                { value: 'ru', text: 'Русский' },
            ],
            validate(value, values) {
                return value !== null && value !== values['native-language'];
            },
        }, {
            id: 'native-language',
            type: 'select',
            text: 'Native language',
            options: [
                { value: 'en', text: 'English' },
                { value: 'de', text: 'Deutsch' },
                { value: 'ru', text: 'Русский' },
            ],
            validate(value, values) {
                return value !== null && value !== values['target-language'];
            },
        }
    ];

    render() {
        return (
            <div className={styles.container}>
                <Settings
                    items={this.settings}
                    value={{
                        ['target-language']: this.props.profile.targetLanguage,
                        ['native-language']: this.props.profile.nativeLanguage,
                    }}
                />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    profile: selectProfile(state),
});

export default connect(
    mapStateToProps,
)(Profile);
