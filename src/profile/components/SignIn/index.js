import React from 'react';
import Button from '../../../components/Button';
import styles from './signin.module.css';

export default class SignIn extends React.Component {
    state = {
        email: '',
    };

    handleEmailChange = (e) => {
        this.setState({
            email: e.target.value,
        });
    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.onSubmit(this.state.email);
    };

    render() {
        return (
            <form
                className={styles.container}
                onSubmit={this.handleSubmit}
            >
                <input
                    className={styles.email}
                    type="text"
                    placeholder="Type your e-mail here.."
                    onChange={this.handleEmailChange}
                />
                <Button
                    className={styles.submit}
                    type="submit"
                    disabled={this.state.email === ''}
                >Sign in</Button>
            </form>
        );
    }
}
