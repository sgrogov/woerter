import Api from '../api';

const initialState = {
    email: null,
    nativeLanguage: null,
    targetLanguage: null,
};

const STATUS_REQUEST = 'PROFILE/STATUS_REQUEST';
const STATUS_SUCCESS = 'PROFILE/STATUS_SUCCESS';
const STATUS_ERROR = 'PROFILE/STATUS_ERROR';

const statusRequest = () => ({ type: STATUS_REQUEST });
const statusSuccess = (profile) => ({
    type: STATUS_SUCCESS,
    payload: { profile },
});
const statusError = () => ({ type: STATUS_ERROR });

export const status = async (dispatch) => {
    dispatch(statusRequest());
    try {
        const profile = await Api.profile.status();
        dispatch(statusSuccess(profile));
    } catch (e) {
        console.error(e);
        dispatch(statusError());
    }
};

const LOGIN_REQUEST = 'PROFILE/LOGIN_REQUEST';
const LOGIN_SUCCESS = 'PROFILE/LOGIN_SUCCESS';
const LOGIN_ERROR = 'PROFILE/LOGIN_ERROR';

const loginRequest = () => ({ type: LOGIN_REQUEST });
const loginSuccess = (profile) => ({
    type: LOGIN_SUCCESS,
    payload: { profile },
});
const loginError = () => ({ type: LOGIN_ERROR });

export const login = async (dispatch, email) => {
    dispatch(loginRequest());
    try {
        const profile = await Api.profile.login(email);
        dispatch(loginSuccess(profile));
    } catch (e) {
        console.error(e);
        dispatch(loginError());
    }
};

export const selectProfile = state => state.profile;
export const selectIsAuthenticated = state => selectProfile(state).email !== null;

export default function profileReducer(state = initialState, action) {
    switch (action.type) {
        case LOGIN_SUCCESS:
        case STATUS_SUCCESS:
            return {
                ...state,
                ...action.payload.profile,
            };
        case LOGIN_ERROR:
        case STATUS_ERROR:
            return {
                ...state,
                email: null,
                nativeLanguage: null,
                targetLanguage: null,
            };
        default:
            return state;
    }
}
