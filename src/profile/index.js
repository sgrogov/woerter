import React from 'react';
import { connect } from 'react-redux';
import { login, selectIsAuthenticated } from './reducer';
import Screen from '../components/Screen';
import Profile from './components/Profile';
import SignIn from './components/SignIn';

class ProfileScreen extends React.Component {
    render() {
        return (
            <Screen
                name="profile"
                heading="Profile"
                backTo="/"
            >
                {this.props.isAuthenticated ? (
                    <Profile />
                ) : (
                    <SignIn onSubmit={this.props.signIn} />
                )}
            </Screen>
        );
    }
}

const mapStateToProps = state => ({
    isAuthenticated: selectIsAuthenticated(state),
});

const mapDispatchToProps = dispatch => ({
    signIn: email => login(dispatch, email),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ProfileScreen);
