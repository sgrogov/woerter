import React from 'react';
import PropTypes from 'prop-types';
import capitalize from 'lodash/capitalize';
import {languages, genders, partsOfSpeech, pronounTypes} from '../../../hardcode';

export default class AddWordForm extends React.Component {
    static propTypes = {
        onChange: PropTypes.func,
    };

    state = {
        language: languages[0],
        partOfSpeech: partsOfSpeech[0].name,
        gender: genders[languages[0]][0],
        pronounType: undefined,
        word: '',
    };

    getInputHandler = (() => {
        const handlers = new Map();

        return (cb) => {
            let handler = handlers.get(cb);
            if (handler === undefined) {
                handler = e => cb(e.target.value);
                handlers.set(cb, handler);
            }
            return handler;
        };
    })();

    onSubmit = (e) => {
        e.preventDefault();

        const form = [
            'language',
            'partOfSpeech',
            'gender',
            'pronounType',
            'word',
        ].reduce((result, property) => ({
            ...result,
            [property]: this.state[property],
        }), {});

        if (typeof this.props.onSubmit === 'function') {
            this.props.onSubmit(form);
        }
    };

    onLanguageChange = (language) => {
        this.setState({language});

        const langGenders = this.getGenders(language);
        if (langGenders.length !== 0 && !langGenders.includes(this.state.gender)) {
            this.onGenderChange(langGenders[0]);
        } else if (langGenders.length === 0 && this.state.gender !== undefined) {
            this.onGenderChange(undefined);
        }
    };
    onPartOfSpeechChange = (partOfSpeech) => {
        this.setState({partOfSpeech});

        switch (partOfSpeech) {
            case 'noun':
                this.onGenderChange(this.getGenders()[0]);
                return;
            case 'pronoun':
                this.onPronounTypeChange(pronounTypes[0]);
                return;
        }
    };
    onWordChange = word => this.setState({word});
    onGenderChange = gender => this.setState({gender});
    onPronounTypeChange = pronounType => this.setState({pronounType});

    getGenders(language = this.state.language) {
        return genders[language];
    }

    render() {
        const langGenders = this.getGenders();
        const pos = partsOfSpeech.find(p => p.name === this.state.partOfSpeech);

        return (
            <form onSubmit={this.onSubmit}>
                <select onChange={this.getInputHandler(this.onLanguageChange)}>
                    {languages.map(lang => (
                        <option
                            key={lang}
                            value={lang}
                        >{capitalize(lang)}</option>
                    ))}
                </select>
                <select onChange={this.getInputHandler(this.onPartOfSpeechChange)}>
                    {partsOfSpeech.map(pos => (
                        <option
                            key={pos.name}
                            value={pos.name}
                        >{capitalize(pos.name)}</option>
                    ))}
                </select>
                {(pos.fields || []).map(field => {
                    switch (field) {
                        case 'gender':
                            return (
                                langGenders.length !== 0 && (
                                    <select
                                        key="gender"
                                        onChange={this.getInputHandler(this.onGenderChange)}
                                    >
                                        {langGenders.map(gender => (
                                            <option
                                                key={gender}
                                                value={gender}
                                            >{gender}</option>
                                        ))}
                                    </select>
                                )
                            );
                        case 'pronounType':
                            return (
                                <select
                                    key="pronounType"
                                    onChange={this.getInputHandler(this.onPronounTypeChange)}
                                >
                                    {pronounTypes.map(ptype => (
                                        <option
                                            key={ptype}
                                            value={ptype}
                                        >{capitalize(ptype)}</option>
                                    ))}
                                </select>
                            );
                    }
                    return null;
                })}
                <input
                    type="text"
                    placeholder="Word"
                    onChange={this.getInputHandler(this.onWordChange)}
                />
                <button type="submit">Add</button>
            </form>
        );
    }
}
