import React from 'react';
import { connect } from 'react-redux';
import Screen from '../components/Screen';
import Api from '../api';
import AddWordForm from './components/AddWordForm';

class ManageScreen extends React.Component {
    state = {
        list: [],
    };

    componentDidMount() {
        Api.words.list().then((result, error) => {
            if (error) return console.warn(error);

            this.setState({
                list: result,
            });
        });
    }

    onAdd = async (word) => {
        try {
            const result = await Api.words.add(word);
            this.list.push(result);
        } catch {
            console.warn('cant add word', word);
        }
    };

    render() {
        return (
            <Screen
                name="manage"
                heading="Manage"
                backTo="/"
            >
                <AddWordForm onSubmit={this.onAdd} />

                <ul>
                {this.state.list.map(word => (
                    <li key={word.value}>
                        {word.value} ({word.gender})
                    </li>
                ))}
                </ul>
            </Screen>
        );
    }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ManageScreen);
