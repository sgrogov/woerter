import React from 'react';
import { Link } from 'react-router-dom';
import styles from './menu.module.css';

export default class Menu extends React.Component {
    render() {
        return (
            <div className={styles.container}>
                <Link
                    to="/learn"
                    className={styles.item}
                >Learn</Link>
                <Link
                    to="/manage"
                    className={styles.item}
                >Manage</Link>
                <Link
                    to="/profile"
                    className={styles.item}
                >Profile</Link>
                <Link
                    to="/settings"
                    className={styles.item}
                >Settings</Link>
                <Link
                    to="/notes"
                    className={styles.item}
                >Notes</Link>
            </div>
        );
    }
}
