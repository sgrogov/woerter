const db = require('./db.js').default;

module.exports.default = {
    words: {
        add(word) {
            return db.query('worter', 'words', collection => new Promise((resolve, reject) => {
                collection.insertOne(word, function(err, result){
                    if (err){
                        reject(err);
                    } else {
                        resolve(result.ops);
                    }
                });
            }));
        },
        list() {
            return db.query('worter', 'words', collection => new Promise((resolve, reject) => {
                collection.find(function(err, result) {
                    if (err){
                        reject(err);
                    } else {
                        resolve(result);
                    }
                });
            }));
        },
    },
};
