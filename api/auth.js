const prefix = 'token-for:';

module.exports.createToken = (user) => {
    return `${prefix}${user.email}`;
};
module.exports.parseToken = (token) => {
    return token.substr(prefix.length);
};
