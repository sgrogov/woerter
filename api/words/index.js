const storage = require('../storage.js').default;

const words = {
    de: new Map([
        ['vater', {
            gender: 'masculine',
            translations: {
                en: 'father',
                ru: 'отец',
            },
        }],
        ['mutter', {
            gender: 'feminine',
            translations: {
                en: 'mother',
                ru: 'мать',
            },
        }],
        ['mädchen', {
            gender: 'neuter',
            translations: {
                en: 'girl',
                ru: 'девочка',
            },
        }],
    ]),
    en: new Map([
        ['father', {
            translations: {
                de: 'vater',
                ru: 'отец',
            },
        }],
        ['mother', {
            translations: {
                de: 'mutter',
                ru: 'мать',
            },
        }],
        ['girl', {
            translations: {
                de: 'mädchen',
                ru: 'девочка',
            },
        }],
    ]),
    ru: new Map([
        ['отец', {
            gender: 'masculine',
            translations: {
                de: 'vater',
                en: 'father',
            },
        }],
        ['мать', {
            gender: 'feminine',
            translations: {
                de: 'mutter',
                en: 'mother',
            },
        }],
        ['девочка', {
            gender: 'neuter',
            translations: {
                de: 'mädchen',
                en: 'girl',
            },
        }],
    ]),
};

function getWord(lang, word) {
    if (!Object.hasOwnProperty.call(words, lang)) return null;
    return words[lang].get(word) || null;
}

module.exports.getWithTranslation = (lang, targetWord, tLang) => {
    const word = getWord(lang, targetWord);

    if (word === null || !Object.hasOwnProperty.call(word.translations, tLang)) return null;

    const translation = getWord(tLang, word.translations[tLang]);

    if (translation === null) return null;

    return {
        ...word,
        word: targetWord,
        translations: undefined,
        translation: word.translations[tLang],
    };
};

module.exports.default = function defineWordsRoutes(router) {
    router.get('/words/list', async (ctx) => {
        if (ctx.user === null) return;

        const list = await (await storage.words.list()).toArray();

        ctx.response.body = list.map(word => ({
            value: word.value,
            gender: word.gender,
        }));
    });

    router.post('/words/add', async (ctx) => {
        if (ctx.user === null) return;

        const word = [
            'language',
            'partOfSpeech',
            'gender',
            'pronounType',
            'word',
        ].reduce((result, property) => ({
            ...result,
            [property]: ctx.request.body[property],
        }), {});

        // check is word already exists

        try {
            await storage.words.add(word);
            ctx.response.status = 200;
            ctx.response.body = word;
        } catch {
            ctx.response.status = 400;
        }
    });
};
