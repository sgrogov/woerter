const { getWithTranslation } = require('../words');

module.exports.default = function defineExercisesRoutes(router) {
    router.get('/exercises', (ctx) => {
        if (ctx.user === null) return;

        ctx.response.body = {
            words: ctx.user.planned.map(({ lang, word }) => getWithTranslation(lang, word, ctx.user.nativeLanguage)),
        };
    });
};
