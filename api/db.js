const MongoClient = require("mongodb").MongoClient;

const url = "mongodb://localhost:27017/";

class MongoDB {
    connect() {
        this.client = new MongoClient(url, { useNewUrlParser: true });

        return new Promise((resolve, reject) => {
            this.client.connect(function (err) {
                if (err) {
                    reject(err);
                }
                resolve();
            });
        });
    }

    disconnect() {
        this.client.close();
    }

    async query(dbName, collectionName, callback) {
        await this.connect();

        const db = this.client.db(dbName);
        const collection = db.collection(collectionName);

        return callback(collection);
    }
}

module.exports.default = new MongoDB();
