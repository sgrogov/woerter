const { createToken, parseToken } = require('../auth');

const users = new Map([
    ['sg.rogov@yandex.ru', {
        email: 'sg.rogov@yandex.ru',
        nativeLanguage: 'en',
        targetLanguage: 'de',
        planned: [
            { lang: 'de', word: 'vater' },
            { lang: 'de', word: 'mutter' },
            { lang: 'de', word: 'mädchen' },
        ],
    }],
]);

function getOrCreateUser(email) {
    let user = users.get(email);
    if (user === undefined) {
        user = {
            email: email,
            nativeLanguage: 'en',
            targetLanguage: 'de',
            planned: [
                { lang: 'de', word: 'vater' },
                { lang: 'de', word: 'mutter' },
                { lang: 'de', word: 'mädchen' },
            ],
        };
        users.set(email, user);
    }
    return user;
}

module.exports.default = function defineUsersRoutes(router) {
    router.use(async (ctx, next) => {
        const token = parseToken(ctx.request.get('Authorization'));
        ctx.user = users.get(token) || null;

        if (ctx.user === null && ctx.originalUrl !== '/users/login') {
            ctx.status = 401;
        } else {
            await next();
        }
    });

    router.get('/users/status', (ctx) => {
        ctx.body = {
            email: ctx.user.email,
            nativeLanguage: ctx.user.nativeLanguage,
            targetLanguage: ctx.user.targetLanguage,
        };
    });

    router.post('/users/login', async (ctx) => {
        const body = ctx.request.body;
        const email = typeof body.email === 'string' ? body.email : null;

        if (email === null) {
            ctx.status = 400;
        } else {
            const user = getOrCreateUser(email);

            ctx.set('authorization', createToken(user));
            ctx.body = {
                email: user.email,
                nativeLanguage: user.nativeLanguage,
                targetLanguage: user.targetLanguage,
            };
        }
    });
};
